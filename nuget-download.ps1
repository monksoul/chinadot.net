﻿echo "";
echo "";
echo "----------------------------------------------";
echo "----Nuget 测试项目 downloads 是否是 projects-----";
echo "----------------------------------------------";
echo "";

Write-Warning "请输入要创建的项目根目录：";
$path = Read-Host "你输入的是";

Write-Warning "请输入要创建的项目数量（int 类型）：";
$count = Read-Host "你输入的是";

Write-Warning "请输入要添加的 Nuget 包名（string 类型）：";
$package = Read-Host "你输入的是";

Write-Warning "请输入项目创建频率边界值（单位秒），比如输入 100，则会从 0 到 100 中随机生成（int类型）：";
$sleep = Read-Host "你输入的是";

# 如果目录不存在就创建
If(!(test-path $path))
{
      New-Item -ItemType Directory -Force -Path $path
}

pushd $path;

# 配置项目模板
$templates = "grpc","webapi","razor","mvc","web","console","worker";

# 配置项目名称常见名
$names = "Hoa","Tina","Abc","Micro","Erp","CMS","WMS","Wps","Adobe","Player","Music","PadNote","Acobat","Winamp","Sales","ProjectManage","CuteFTP","CCTV","Freehand","Flash","Express","UMail","Koomail","Express2";

for ($i = 0; $i -le [int]$count; $i++)
{
    # 获取随机模板
    $j = Get-Random -Minimum 0 -Maximum $templates.Length;
    $template = $templates[$j];
    Write-Warning "当前创建的项目模板是：$template"

    # 获取随机项目名称
    $z = Get-Random -Minimum 0 -Maximum $names.Length;
    $name = $names[$z];
    Write-Warning "当前创建的项目名称是：$name"

    # 定义项目路径
    $project_path = "$path/$name$i$j$z";
    mkdir $project_path;
    pushd $project_path;

    # 创建项目并安装 Nuget 包并还原包
    dotnet new $template;
    dotnet add package $package;
    dotnet build;

    # 配置休眠间隔
    $n = Get-Random -Minimum 0 -Maximum $sleep;

    Write-Warning "休眠 $n 秒......";
    Start-Sleep -Seconds $n;
    Write-Warning "休眠结束，程序继续......";
}
pushd $path;
Write-Warning "全部生成成功";